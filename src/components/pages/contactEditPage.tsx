import { Box, TextField } from "@mui/material";
import { useParams } from "react-router-dom";
import { useContactEdit } from "@hooks/contacts/useContactEdit";
import { useEffect, useState } from "react";

function ContactEditPage() {
  const { id } = useParams();

  const { contact } = useContactEdit(id);
  const [firstName, setFirstName] = useState<String>("");
  const [lastName, setLastName] = useState<String>("");
  const [email, setEmail] = useState<String>("");

  useEffect(() => {
    if (contact) {
      setFirstName(contact.firstName);
      setLastName(contact.lastName);
      setEmail(contact.email);
    }
  }, [contact]);

  return (
    <Box>
      <TextField variant="outlined" id={`firstName`} value={firstName} />
      <TextField variant="outlined" id={`lastName`} value={lastName} />
      <TextField variant="outlined" id={`email`} value={email} />
    </Box>
  );
}

export default ContactEditPage;
