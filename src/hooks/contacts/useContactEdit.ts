import { useEffect, useState } from "react";
import { IPerson } from "@lib/models/person";
import { useCallback } from "react";
import { contactsClient } from "../../lib/clients/contacts";

export function useContactEdit(id: string) {
  console.log(`Getting contact by id ${id}`);
  const [contact, setContact] = useState<IPerson>();

  useEffect(() => {
    const getContact = async () => {
      const fetchedContact = await contactsClient.getContactById(id);
      setContact(fetchedContact);
    };
    getContact();
  }, []);

  return {
    contact: contact as IPerson,
    update: useCallback((updated: IPerson) => {
      console.log(updated);
      throw new Error("Not implemented!");
    }, [])
  };
}
